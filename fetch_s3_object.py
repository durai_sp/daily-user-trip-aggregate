"""
Fetch S3 Object.

Author: Durai S
email: dsankaran@agero.com
Date: Jul 10, 2017

FetchS3Object class has the functionality to fetch data from S3 and put it in Firehose Stream.
"""


from platformutils.s3_utils import S3Utils
from platformutils.utils.time_util import TimeUtil
from platformutils.firehose_utils import FirehoseUtils
from io import BytesIO
from gzip import GzipFile
import csv
import json

class FetchS3Object(object):

    def __init__(self):
	self.s3 = S3Utils('us-east-1', 'DEBUG')
	self.firehose = FirehoseUtils('user-aggregate-stream', 'us-east-1', 'DEBUG')
	self.time =TimeUtil()
	self.bucket = 'policypalacnprod'
	self.key_path = 'agero/prod/score/daily-user-trip-aggregate/daily-user-14day-aggregate/'
	self.dict = ['userId', 'deviceModel', 'dateCol', 'overallScore', 'overallScoreBin', 'overallScoreType', 'numTrips', 'abcAgg', 'abcBin', 'abcScore', 'numABC', 'phoneAgg', 'phoneBin', 'phoneScore', 'numPhone', 'speedAgg', 'speedBin', 'speedScore', 'numGeoCode']

    def handle_data(self):
	"""
	This method is used to get object from S3.
	Process the object which is get from S3
        """
	utc_object = self.s3.is_key_available(self.bucket, self.key_path + str(self.time.get_utc_timestamp(1, 'ms')) + '_aggOuput.csv.gz')
	if utc_object:
	    self.fetch_object(self.key_path + str(self.time.get_utc_timestamp(1, 'ms')) + '_aggOuput.csv.gz')
	date_object = self.s3.is_key_available(self.bucket, self.key_path + str(self.time.get_date(1)) + '_aggOuput.csv.gz')
	if date_object:
	    self.fetch_object(self.key_path + str(self.time.get_date(1)) + '_aggOuput.csv.gz')


    def fetch_object(self, key):
	"""
	This method is used to fetch object from S3.
        :param key: Provide key has to be get the content from S3.
        """
	content = self.s3.get_object_content(self.bucket, key)
	bytestream = BytesIO(content)
	data = GzipFile(None, 'rb', fileobj=bytestream).read().decode('utf-8')
	batch_size = 0
	records = []
	for row in csv.DictReader(data.splitlines()):
	    handled_d = self.parse_row(row)
	    records.append(handled_d)
	    batch_size = batch_size + 1
	    if batch_size == 500:
		self.put_record(records)
		records = []
		batch_size = 0
	if batch_size > 0:
	    self.put_record(records)	
	    
    def parse_row(self, line):
	"""
	This method is used to parse single row which is from S3 object.
        :param line: Provide line has to be parse.
        """
	r = json.dumps(line)
	line_value = json.loads(r)
	it_writer = ''
	for k in self.dict:
	    if k in line_value and 'deviceModel' in k:
		it_writer += line_value[k].replace(',', '#') + ','
	    else:
		it_writer += line_value[k] + ','

	return {'Data' : it_writer[:-1] + "\n"}

    def put_record(self, records):
	"""
	This method is used to load records to Firehose Stream.
        :param records: Provide records has to be load.
        """
	self.firehose.put_records(records)
