"""
Dashboard.

Author: Durai S
email: dsankaran@agero.com
Date: Jul 10, 2017

Dashboard class has the functionality to fetch S3 object.
"""
from fetch_s3_object import FetchS3Object

def handler():
    fetch = FetchS3Object()
    fetch.handle_data()

handler()
