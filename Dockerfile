FROM ubuntu:latest

RUN apt-get update
RUN apt-get install -y python python-pip wget
RUN pip install --upgrade pip
RUN pip install boto3
RUN pip install psycopg2

COPY platformutils /platformutils
ADD dashboard.py /
ADD fetch_s3_object.py /


CMD [ "python", "./dashboard.py"]




